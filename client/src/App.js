import React from 'react'
import Body from './components/Body/Body'
import s from './App.module.scss'
import Groups from "./components/Groups/Groups";
import Description from "./components/Description/Description";
import ModalWindow from "./components/ModalWindow/ModalWindow";


function App() {
  return (
      <div className={s.wrapper}>
        <div className={s.phoneBook}>
          <div className={s.phoneBook__row}>
           <Body/>
           <Groups/>
           <Description/>
          </div>
          <ModalWindow/>
        </div>
      </div>
  )
}

export default App
