import React from 'react'
import  s from './MobileItem.module.scss'


const MobileItem = ( {...item} ) => {

  return (
    <div className={ s.phone__column }>
      <label htmlFor={"mobile"}>Mobile phone</label>
      <input className={ s.phone__txt_field } type="text" name="mobile"/>
    </div>
  )
}

export default MobileItem