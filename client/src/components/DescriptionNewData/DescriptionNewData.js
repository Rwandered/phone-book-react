import React from 'react'
import s from './DescriptionNewData.module.scss'
import DescriptionNewDataForm from "../DescriptionNewDataForm/DescriptionNewDataForm";

const DescriptionNewData = () => {


  return (
    <div
      className={ s.false__field__wrapper }
      data-active
    >
      <DescriptionNewDataForm/>
    </div>
  )

}

export default DescriptionNewData