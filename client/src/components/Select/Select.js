import React from 'react'
import s from './Select.module.scss'


const groups =  [
  { id: 1, name: 'All Contacts', type: 'system' },
  { id: 2, name: 'Family', type: 'system' },
  { id: 3, name: 'Friends', type: 'system' },
  { id: 4, name: 'Co-Workers', type: 'system' }
]

const Select = () => {

  return (
    <select name="group" className={ s.group__select }>
     {
        groups.map(  ( {id, name, type } ) => (
          <option
            value={name}
            key={id}
          >{name}
          </option>
        ))
     }
    </select>
  )
}

export default Select