import React from 'react'
import cn from 'classnames'
import s from './ModalBody.module.scss'
import ModalSlider from "../ModalSlider/ModalSlider";


const ModalBody = () => {

  return (
   <>
     <form
      className={ s.form__flex__row }
      encType={'multipart/form-data'}
     >
       <div className={ cn(s.form__column, s.form__logo, s.logo, s.logo__row) }>
         <div className={ s.logo__img }/>
         <div className={ s.insert__img }>
           <input
             className= { s.phone__img_field }
             type="file"
             name="logo"
             style={ {opacity:0} }
             data-card-logo=""
           />
         </div>
       </div>
       <div className={ cn(s.form__column, s.phone, s.phone__row) }>
        <ModalSlider/>
       </div>


     </form>
   </>
  )
}

export default ModalBody


