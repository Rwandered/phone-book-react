import React from 'react'
import cn from 'classnames'
import s from './Groups.module.scss'
import GroupsList from "../GroupsList/GroupsList";
import GroupsSetting from "../GroupsSetting/GroupsSetting";
import appS from '../../App.module.scss'


const Groups = () => {


  return (
    <div className={ cn(appS.phoneBook__column, s.phoneBook__groups, s.groups)}>
      <div className={s.groups__row}>
        <div className={ cn( s.groups__column, s.groups__newGroup) }/>
        <GroupsList/>
        <GroupsSetting/>
      </div>
    </div>
  )
}

export default Groups