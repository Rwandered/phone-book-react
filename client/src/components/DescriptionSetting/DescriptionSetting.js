import React from 'react'
import  cn  from 'classnames'
import s from './DescriptionSetting.module.scss'


const DescriptionSetting = () => {

  return (
    <div className={ cn(
      s.description__column,
      s.description__setting
    )}>

      <div className={ cn(
        s.setting_column,
        s.description__newphone
      )}
        data-new-phone-field={'true'}
      />
      <div className={ cn(
        s.setting_column,
        s.description__editcard
      )}
        data-edit-card={'true'}
      />
    </div>
  )
}

export default DescriptionSetting