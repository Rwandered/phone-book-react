import React from 'react'
import cn from 'classnames'
import s from './DescriptionLogo.module.scss'

const DescriptionLogo = () => {

  return (
    <React.Fragment>
      <div className={
        cn (
          s.description__column,
          s.description__userphoto
        )
      }/>
    </React.Fragment>
  )
}


export default DescriptionLogo