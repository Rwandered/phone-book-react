import React from 'react'
import cn from 'classnames'
import s from './Card.module.scss'


const Card = ( { card } ) => {

  const { firstName, lastName, info  } = card

  return (
    <div className={ cn( s.cards__card, s.card )} data-id="1" data-card="">

      <div className={ cn( s.card__col, s.card__img )}/>

      <div className={ cn( s.card__col, s.card__info )}>
        <p className={s.name}><strong>{ firstName} {lastName}</strong></p>
        <p className={ s.info }><strong>{ info}</strong></p>
      </div>

      <div className={ cn( s.card__col, s.card__number) }>
        <p><strong>+49 176 458 4587</strong></p>
      </div>
    </div>
  )
}

export default Card