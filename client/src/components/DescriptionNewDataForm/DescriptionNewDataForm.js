import React from 'react'
import s from './DescriptionNewDataForm.module.scss'

const DescriptionNewDataForm = () => {


  return (
    <form>
      <input
        className={ s.false_field }
        type="text"
        value=""
        readOnly={true}
        name="typeNumber"
        placeholder="Type of number"
      />
      <input
        className={ s.false_field}
        type="tel"
        value=""
        readOnly={true}
        name="number"
        placeholder="Number"
      />
      <div
        className={s.false_field_ok_btn}
        data-field-ok="true"
      />
      <div
        className={s.false_field_cancel_btn}
        data-field-cancel="true"/>
    </form>
  )
}

export default DescriptionNewDataForm