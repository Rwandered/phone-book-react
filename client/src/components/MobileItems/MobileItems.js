import React from 'react'
import s from './MobileItems.module.scss'
import MobileItem from '../MobileItem/MobileItem'


// 1) нужно разбить исходный массив с номерами на массив, каждый элемент которого представляется массив максимум из 4
// объектов с номерами
// 2) далее в компонент MobileItem передаем подмассив и получаем компонент слайдера по 4 номера

const MobileItems = ( {...items} ) => {


  return (
    <div className={s.slider__item}>
      {
        items.items.map( item => <MobileItem item={ item }/> )
      }
    </div>
  )
}

export default MobileItems