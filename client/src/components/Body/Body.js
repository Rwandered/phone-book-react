import React from 'react'
import cn from 'classnames'
import appS from '../../App.module.scss'
import BodyHeader from '../BodyHeader/BodyHeader'
import Cards from '../Cards/Cards'
import s from './Body.module.scss'


const Body = () => {

  return (
    <div className={ cn(appS.phoneBook__column, s.phoneBook__body, s.body) }>
      <BodyHeader/>
      <Cards/>
    </div>
  )
}

export default Body