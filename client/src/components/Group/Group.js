import React from 'react'
import cn from 'classnames'
import s from './Group.module.scss'
// import InputNewGroup from "../InputNewGroup/InputNewGroup";


const Group = ( {groupId, groupType, groupName} ) => {
  return (
    <li data-list-el="" data-group-type= { groupType } data-removable="true" data-group-id={groupId}>
      {/*<InputNewGroup/>*/}
      <p className={s.listElValue}>{ groupName }</p>
      <div className={
        cn(
          s.groups__controls_wrapper,
          s.groups__controls_wrapper_hide,
          s.disappear
      )}>
        <div className={s.groups__edit_controls}>
          <div className={s.rename_group}/>
          <div className={s.delete_group}/>
        </div>
      </div>
    </li>
  )
}

export default Group