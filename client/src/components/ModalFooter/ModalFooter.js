import React from 'react'
import cn from 'classnames'
import s from './ModalFooter.module.scss'


const ModalFooter = () => {

  return (
    <div className={cn( s.modal__footer, s.modal__column) }>
      <div className={ s.modal__ok_btn }/>
      <div className={ s.modal__cancel_btn }/>
    </div>
  )
}

export default ModalFooter