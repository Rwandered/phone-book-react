import React from 'react'
import cn from 'classnames'
import s from './Description.module.scss'
import DescriptionHeader from "../DescriptionHeader/DescriptionHeader";
import DescriptionLogo from "../DescriptionLogo/DescriptionLogo";
import DescriptionInfo from "../DescriptionInfo/DescriptionInfo";
import DescriptionNewData from "../DescriptionNewData/DescriptionNewData";
import DescriptionSetting from "../DescriptionSetting/DescriptionSetting";

const Description = () => {

  return (
    <div className={ cn(s.phoneBook__column, s.phoneBook__description, s.description ) }>
      <div
        className={ s.description__row }
        data-id={'1'}
      >
        <DescriptionHeader/>
        <DescriptionLogo/>
        <DescriptionInfo/>
        <DescriptionNewData/>
        <DescriptionSetting/>
      </div>
    </div>
  )
}


export default Description
