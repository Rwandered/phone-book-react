import React from 'react'
import s from './GroupsList.module.scss'
import Group from "../Group/Group";
// import InputNewGroup from "../InputNewGroup/InputNewGroup";

const groups =  [
  // { id: 1, name: 'All Contacts', type: 'system' },
  { id: 2, name: 'Family', type: 'system' },
  { id: 3, name: 'Friends', type: 'system' },
  { id: 4, name: 'Co-Workers', type: 'system' }
]

const GroupsList = () => {

  // const { groups } = props

  return (
    <div className={ s.groups__listGroup }>
      <ul>
        <li data-all-contact="true" data-list-el="" data-group-type="system">
          <p className="list-el-value">All Contacts</p>
        </li>
        { groups.map( group => <Group key={group.id} groupType={group.type} groupName={group.name}/>)}
        <li data-false-input="">Этот элемент списка скрыт
          {/*<InputNewGroup/>*/}
        </li>
      </ul>
    </div>
  )
}

export default GroupsList