import React from 'react'
import cn from 'classnames'
import s from './DescriptionHeader.module.scss'

const DescriptionHeader = () => {


  return (
    <div className={ cn( s.description__column, s.description__username )} >
      <div className={ s.userName }>
        John <br/> Smith
      </div>
      <div className={s.userGroup }/>
    </div>
  )
}

export default DescriptionHeader
