import React from 'react'
// import cn from 'classnames'
import s from './ModalSlider.module.scss'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Slider from 'react-slick'
import Select from '../Select/Select'
import MobileItems from "../MobileItems/MobileItems";


const numbers = [
  { type: 'mobile', number: '+996 946 01 16' },
  { type: 'work', number: '+996 946 01 16' },
  { type: 'home', number: '+996 946 01 14' },
  { type: 'home', number: '+996 946 01 15' },
  { type: 'home', number: '+996 946 01 11' },
  { type: 'home', number: '+996 946 01 12' },
 ]

const arraysWithItems = numbers.reduce( (acc,number)=>{
  if(acc[acc.length-1].length === 4){
    acc.push([])
  }
  acc[acc.length-1].push(number)
  return acc
}, [[]])

console.log(arraysWithItems)


// [
//   [ { type: 'mobile', number: '+996 946 01 16' },
//     { type: 'work', number: '+996 946 01 16' },
//     { type: 'home', number: '+996 946 01 14' },
//     { type: 'home', number: '+996 946 01 15' },],
//     [ { type: 'mobile', number: '+996 946 01 16' },
//       { type: 'work', number: '+996 946 01 16' },
//       { type: 'home', number: '+996 946 01 14' },
//       { type: 'home', number: '+996 946 01 15' },],
//
// ]


const ModalSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  }

  return (
    <Slider style={ {width: '300px'} } {...settings}>
      <div className={s.slider__item}>
        <div className={s.phone__column}>
          <label htmlFor={'firstName'} >First name</label>
          <input
            className={s.phone__txt_field}
            type={'text'}
            name={'firstName'}
            required=""
          />
        </div>
        <div className={s.phone__column}>
          <label htmlFor="lastName">Last name</label>
          <input
            className={s.phone__txt_field}
            type="text"
            name="lastName"
            required=""
          />
        </div>
        <div className={s.phone__column}>
          <label htmlFor="group">Group</label>
          <Select/>
        </div>
        <div className={ s.phone__column }>
          <label htmlFor="info">Some information</label>
          <input className="phone__txt_field" type="text" name="info"/>
        </div>
      </div>
      {/*этот блок тоже отдельный компонент
      , так как номера телефонов может быть не определенное количество*/}
      {
        arraysWithItems.map( (arrayWithItems) => <MobileItems items={ arrayWithItems }/> )
      }
    </Slider>
  )
}

export default ModalSlider
