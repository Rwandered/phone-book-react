import React from 'react'
import cn from 'classnames'
import s from './GroupsSetting.module.scss'

const GroupsSetting = () => {
  return (
    <div className={ cn(
      s.groups__column,
      s.groups__setting
    )}>
      <div className={ cn( s.setting_column, s.setting__img) } data-setting=""/>
      <div className={ cn( s.setting_column, s.edit__img) } data-edit="" style={ {display: 'inlineBlock'} }/>
      <div className={ cn( s.setting_column, s.edit__stop__img) } data-edit-stop="" style={ {display: 'none'}}/>
    </div>
  )
}

export default GroupsSetting