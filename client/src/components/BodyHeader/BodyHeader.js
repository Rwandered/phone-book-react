import React from 'react'
import cn from 'classnames'
import s from './BodyHeader.module.scss'


const BodyHeader = () => {

  return (
    <div className={ cn(s.body__header, s.header)}>
      <div className={cn(s.header__column, s.header__img)}>
        <img src="../../assets/img/groupLogo.png" alt="gpL"/>
        <p className={ s.header__img_groupName }>You phone book</p>
      </div>
      <div className={ cn( s.header__column, s.header__search) }>
        <input className={ s.search__string } type="text"/>
      </div>
      <div className={cn( s.header__column, s.header__menu) }>
        <img className={s.menu__img} src="../../assets/img/menu.png" alt="Menu"/>
      </div>
    </div>
  )
}

export default BodyHeader