import React from 'react'
import cn from 'classnames'
import s from './DescriptionInfo.module.scss'

const DescriptionInfo = () => {


  return (
    <div className={ cn(s.description__column, s.description__userinfo) }>
      <p>Mobile phone:</p>
      <p>+996 946 01 16</p><p>Work phone:</p>
      <p>+996 946 01 16</p><p>Home phone:</p>
      <p>+996 946 01 16</p>
    </div>
  )
}

export default DescriptionInfo