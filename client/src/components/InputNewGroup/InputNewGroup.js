import React from 'react'
import s from './InputNewGroup.module.scss'


const InputNewGroup = () => {

  return (
    <input
      className={ s.inp_new_group }
      type="text"
      maxLength={15}
      placeholder={'Max: 15 symbols'}
    />
  )
}

export default InputNewGroup