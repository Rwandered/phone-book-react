import React from 'react'
import cn from 'classnames'
import s from './ModalHeader.module.scss'


const ModalHeader = ( {modalTitle} ) => {

  return (
    <div className={cn( s.modal__header, s.modal__column) }>
      <span className={ s.modal__title }>{ modalTitle || 'Modal window' }</span>
      <span className={ s.modal__close }> × </span>
    </div>
  )
}

export default ModalHeader