import React from 'react'
import s from './ModalWindow.module.scss'
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalBody from "../ModalBody/ModalBody";

const ModalWindow = () => {

  return (
    <div className={s.modal__layer}>
      <div className={s.modal__window}>
        <div className={s.modal__row}>
          <ModalHeader/>
          <ModalBody/>
          <ModalFooter/>
        </div>
      </div>
    </div>
  )
}

export default ModalWindow