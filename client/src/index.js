import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './styles.scss'


ReactDOM.render(
  <React.Fragment>
    <App />
  </React.Fragment>,
  document.getElementById('root')
);

